package com.grupoupc.applistaspersonalizadas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.grupoupc.applistaspersonalizadas.adapter.PokemonAdapter;
import com.grupoupc.applistaspersonalizadas.databinding.ActivityMainBinding;
import com.grupoupc.applistaspersonalizadas.model.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //MainActivity = ActivityMain + Binding   <>
    ActivityMainBinding binding;

    private List<Pokemon> pokemons = new ArrayList<>();  //numero de elementos = 0
    private PokemonAdapter adaptador;  //null

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loadData(); //numero de elementos = 3
        setUpAdapter();

    }

    private void setUpAdapter() {

        adaptador = new PokemonAdapter(pokemons); // adaptador ya esta instanciado
        binding.rvPokedex.setAdapter(adaptador);
        binding.rvPokedex.setLayoutManager(new LinearLayoutManager(this));
        //binding.rvPokedex.setLayoutManager(new GridLayoutManager(this,4));
    }

    private void loadData() {

        pokemons.add(new Pokemon("Bulbasaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"));  //elementos = 1
        pokemons.add(new Pokemon("Ivysaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"));
        pokemons.add(new Pokemon("Venusaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"));
    }

}