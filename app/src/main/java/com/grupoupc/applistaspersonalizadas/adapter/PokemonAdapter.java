package com.grupoupc.applistaspersonalizadas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.grupoupc.applistaspersonalizadas.R;
import com.grupoupc.applistaspersonalizadas.databinding.ItemPokedexBinding;
import com.grupoupc.applistaspersonalizadas.model.Pokemon;

import java.util.List;

//2. Necesitamos implementar los metodos del adaptador
public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonAdapterViewHolder> {

    List<Pokemon> pokemons;

    public PokemonAdapter(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    //4. Presentar con que XML va a trabajar el adaptador
    @NonNull
    @Override
    public PokemonAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //view = item_pokedex.xml
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokedex,parent,false);
        return new PokemonAdapterViewHolder(view);
    }

    //5. Se va a ejecutar tantas veces elementos tenga la lista
    @Override
    public void onBindViewHolder(@NonNull PokemonAdapterViewHolder holder, int position) {

        /*
        pokemons.add(new Pokemon("Bulbasaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"));  //elementos = 1
        pokemons.add(new Pokemon("Ivysaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"));
        pokemons.add(new Pokemon("Venusaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"));
         */

        Pokemon pokemon = pokemons.get(position); //2
        holder.bind(pokemon);

    }

    //3. Cuantos elementos tiene tu lista
    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    //1. Una clase interna = ViewHolder
    class PokemonAdapterViewHolder extends RecyclerView.ViewHolder {

        ItemPokedexBinding binding; //null

        //itemView = item_pokedex.xml
        public PokemonAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemPokedexBinding.bind(itemView);
        }

        void bind(Pokemon pokemon){

            binding.tvNombrePokemon.setText(pokemon.getNombre());
        }


    }

}
